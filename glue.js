var methods = {
	preload : preload,
	create : create,
	update : update
};
var game = new Phaser.Game(800, 600, Phaser.AUTO, 'glue-game', methods);

var h_houses = 8;
var v_houses = 6;
var houses_width = 70;
var houses_height = houses_width;

var houses_00_ref = {
	x : 150,
	y : 100
};

var houses_points = new Array();

for (h = 0; h < h_houses; ++h) {
	houses_points[h] = new Array();
	for (v = 0; v < v_houses; ++v) {
		p_x = houses_00_ref.x + (h * houses_width);
		p_y = houses_00_ref.y + (v * houses_height);
		houses_points[h][v] = new Phaser.Point(p_x, p_y);
	}
}

var current_score = 0;
var best_score = current_score;

var whiteStar_current_house = {
	h : 2,
	v : 2
}; // player
var blackStar_current_house = {
	h : 2,
	v : 0
}; // enemy

//diferencial distance
var whiteStar_d_x = 10;
var whiteStar_d_y = 10;

var whiteStar_current_point = houses_points[whiteStar_current_house.h][whiteStar_current_house.v];
var blackStar_current_point = houses_points[blackStar_current_house.h][blackStar_current_house.v];

var initial_step_jump = 100;
var initial_step_pause = 1000;

var step_jump = initial_step_jump;
var step_pause = initial_step_pause;

var text_current_score;
var text_best_score;

var whiteStar_sprite;
var blackStar_sprite;

var cursors;

var whiteStar_tween;

function preload() {
	game.load.image('white_star', 'assets/white_star.png');
	game.load.image('black_star', 'assets/black_star.png');
	game.load.image('arrow_keys', 'assets/arrow_keys.png');
}

// this is just for a visual perception of the grid points
function show_grid() {
	var graphics = game.add.graphics(0, 0);

	graphics.lineStyle(1, 0xFFFFFF, 1);

	houses_points.forEach(function(h, hk) {
		h.forEach(function(v, vk) {
			graphics.moveTo(v.x, v.y);
			graphics.lineTo(v.x + houses_width, v.y);

			graphics.moveTo(v.x, v.y);
			graphics.lineTo(v.x, v.y + houses_height);
		});
	});
}

function create() {

	game.add.text(10, 10, 'Glue', {
		font : "32px Arial",
		fill : '#FFFFFF'
	})
	game.add.text(10, 100, 'Score: ', {
		font : "16px Arial",
		fill : '#FFFFFF'
	})
	game.add.text(20, 130, 'Best: ', {
		font : "16px Arial",
		fill : '#FFFFFF'
	})

	text_current_score = game.add.text(60, 100, current_score, {
		font : "18px Arial",
		fill : '#FFFFFF'
	})
	text_best_score = game.add.text(60, 130, best_score, {
		font : "18px Arial",
		fill : '#FFFFFF'
	})

	//show_grid(); // displays a rough grid just for debug/reference only

	game.add.sprite(630, 490, 'arrow_keys');

	whiteStar_sprite = game.add.sprite(whiteStar_current_point.x + whiteStar_d_x, whiteStar_current_point.y + whiteStar_d_y, 'white_star');
	blackStar_sprite = game.add.sprite(blackStar_current_point.x, blackStar_current_point.y, 'black_star');

	whiteStar_tween = game.add.tween(whiteStar_sprite);

	start_listeners();

	blackStar_move();
}

function start_listeners() {
	bd = document.getElementById('eBody');
	bd.addEventListener('keydown', trigger_whiteStar_move, false);
}

function stop_listeners() {
	bd = document.getElementById('eBody');
	bd.removeEventListener('keydown', trigger_whiteStar_move, false);
}

function trigger_whiteStar_move(evt) {

	var move = false;

	if (evt.keyCode == 38 && (whiteStar_current_house.v > 0)) { // arrow up
        --whiteStar_current_house.v;
        move = true;
	};

	if (evt.keyCode == 40 && (whiteStar_current_house.v < (v_houses - 1))) { // arrow down
        ++whiteStar_current_house.v;
        move = true;
	};

	if (evt.keyCode == 39 && (whiteStar_current_house.h < (h_houses - 1))) { // arrow right
		++whiteStar_current_house.h;
		move = true;
	};

	if (evt.keyCode == 37 && (whiteStar_current_house.h > 0)) { // arrow left
		--whiteStar_current_house.h;
		move = true;
	};

	if (move) whiteStar_move();
}

function check_collision() {

	H = whiteStar_current_house.h == blackStar_current_house.h;
	V = whiteStar_current_house.v == blackStar_current_house.v;

	if (H && V) {
		++current_score;
	} else {
		if (current_score > 0)
			--current_score;
	}

	if (current_score > best_score) {
		best_score = current_score;
	}

	var level = 0;

	if (current_score > 3) {
		++level;
		if (current_score > 6) {
			++level;
			if (current_score > 9) {
				++level;
				if (current_score > 12) {
					++level;
					if (current_score > 15) {
						++level;
						if (current_score > 20) {
							++level;
							if (current_score > 25) {
								//++level; // a partir deste ponto a performance arrebenta :(
							}
						}
					}
				}
			}
		}
	}

	step_jump = initial_step_jump - (level * 15);
	step_pause = initial_step_pause - (level * 60);
}

function pause_blackStar_move(event, context, ns) {
	game.time.events.add(step_pause, this.blackStar_move, this);
}

function whiteStar_move() {

	stop_listeners();

	var whiteStar_point = houses_points[whiteStar_current_house.h][whiteStar_current_house.v];

	var next_x = whiteStar_point.x + whiteStar_d_x;
	var next_y = whiteStar_point.y + whiteStar_d_y;

	var whiteStar_tween = game.add.tween(whiteStar_sprite);
	whiteStar_tween.to({
		x : next_x,
		y : next_y
	}, step_jump);
	whiteStar_tween.onComplete.add(start_listeners, this);
	whiteStar_tween.start();
}

function blackStar_move() {

	check_collision();

	move_blackStar_house();

	var blackStar_point = houses_points[blackStar_current_house.h][blackStar_current_house.v];

	var next_x = blackStar_point.x;
	var next_y = blackStar_point.y;

	var blackStar_tween = game.add.tween(blackStar_sprite);
	blackStar_tween.to({
		x : next_x,
		y : next_y
	}, step_jump);
	blackStar_tween.onComplete.add(pause_blackStar_move, this);
	blackStar_tween.start();
}

function move_blackStar_house() {

	h_or_v = HorV();

	if (h_or_v == 'H') {
		if (blackStar_current_house.h < 1) {
			blackStar_current_house.h = 1;
		} else if (blackStar_current_house.h > (h_houses - 2)) {
			blackStar_current_house.h = h_houses - 2;
		} else {
			l_or_r = LorR();

			if (l_or_r == 'L') {
				--blackStar_current_house.h;
			} else {
				++blackStar_current_house.h;
			}
		}
	} else {
		if (blackStar_current_house.v < 1) {
			blackStar_current_house.v = 1;
		} else if (blackStar_current_house.v > (v_houses - 2)) {
			blackStar_current_house.v = v_houses - 2;
		} else {
			u_or_d = UorD();

			if (u_or_d == 'U') {
				--blackStar_current_house.v;
			} else {
				++blackStar_current_house.v;
			}
		}
	}
}

function update() {
	text_current_score.text = current_score;
	text_best_score.text = best_score;
}

function rand(max) {
	return Math.floor(Math.random() * max);
}

// Horizontal or Vertical ?
function HorV() {
	r = rand(2); // will return 0 or 1

	if (r == 1)
		return 'H';

	return 'V';
}

// Left or Right ?
function LorR() {
	r = rand(2);

	if (r == 1)
		return 'L';

	return 'R';
}

// Up or Down ?
function UorD() {
	r = rand(2);

	if (r == 1)
		return 'U';

	return 'D';
}

function check_keys() {

	if (whiteStar_tween.isRunning)
		return;

	if (cursors.up.isDown && whiteStar_current_house.v > 0) {
		--whiteStar_current_house.v;
	}

	if (cursors.down.isDown && whiteStar_current_house.v < (v_houses - 1)) {
		++whiteStar_current_house.v;
	}

	if (cursors.left.isDown && whiteStar_current_house.h > 0) {
		--whiteStar_current_house.h;
	}

	if (cursors.right.isDown && whiteStar_current_house.h < (h_houses - 1)) {
		++whiteStar_current_house.h;
	}

	var whiteStar_point = houses_points[whiteStar_current_house.h][whiteStar_current_house.v];

	var next_x = whiteStar_point.x;
	var next_y = whiteStar_point.y;

	whiteStar_tween.to({
		x : next_x,
		y : next_y
	}, step_jump);
	whiteStar_tween.start();
}
